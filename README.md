# Waveglider Data Extraction

This project is a java client application that connects to the Liquid Robotics Wave Glider Management System,
extracts data for various platforms and then publishes them to the ODSS communication backplane. In order to
build this project, you have to install Apache CXF and set an environment variable called "CXF_HOME" to the
location where you installed it.  CXF is used to take the WSDL defined by LR and it generates the client
code to interact with that service.  Here are the steps that I took to build:

1. Got an account and a URL for the WGMS system from Liquid Robotics.
2. Got the URL or the WSDL defintion for the service.
3. Downloaded and installed [Apache-CXF](http://cxf.apache.org/)
4. Ran the WSDL2JAVA tool from Apache-CXF using the following (where CXF_HOME is where you installed CXF):

```bash
CXF_HOME/bin/wsdl2java -client -frontend jaxws21 -all -ant -p org.mbari.odss.waveglider.client.wgms.mbari -d src https://mbari.wgms.com/webservices/entityAPI.asmx?WSDL
CXF_HOME/bin/wsdl2java -client -frontend jaxws21 -all -ant -p org.mbari.odss.waveglider.client.wgms.vr2ctest -d src https://vr2ctest.wgms.com/webservices/entityAPI.asmx?WSDL
```

This generated all the source code in the org.mbari.odss.waveglider.client.wgms package as well as the build.xml file (originally in the src directory, moved manually). In order to get this to work, you have to import the SSL certificate into a keystore and then specify that keystore when you run the client.  To import the SSL certificate, I went to the WSDL URL above and then used Firefox to export the SSL certificate to a .pem file.  Then using keytool, I created a new keystore using:

```bash
keytool -import -alias mbariwgmsclient -file mbari.wgms.com.pem -storepass wgmsclient -keystore mbari.wgms.keystore
```

After that, when you want to run the client, you have to specify the keystore using the command line properties (see bin/waveglider.sh):

```bash
-Djavax.net.ssl.trustStore=mbari.wgms.keystore -Djavax.net.ssl.keyStorePassword=wgmsclient
```

NOTE: In June of 2015, Liquid updated their SSL certificate. It took a couple weeks, but then, all of a sudden, the software stopped working.  After MUCH thrashing, I had to generate a new keystore using the above commands, but using a different alias and keystore names.  Then things started working again. Also note that I think I had to use the full path to the keytool as the default one on the machine I was running this on was different than the one I was using to run the software.

NOTE: One of the tasks when adding a new vehicle is to find the vehicle ID and entity type.  This was amazingly hard to figure out, but if you log into the Pilot Portal, select the vehicle top left drop down box and then click on the icon (it changes based on the org you are logged into) to the right of the drop down, you will get a new window which will have the vehicle ID in the URL ... sigh.

NOTE: On August 19, 2020, the same thing happened again and I used Firefox to browse to the [WSDL Site](https://mbari.wgms.com/webservices/entityAPI.asmx?WSDL). I then clicked on the padlock in the address bar and clicked on the arrow next to 'Connection Secure' and then clicked on 'More Information'. This opened a new window and I clicked on the 'Security' tab, then on 'View Certificate'. That opens a new web page and about halfway down the page there is a link next to 'Download' where you can download the wgms-com.pem file.  Once I downloaded it on my laptop, I SFTP'd it over to pismo in the /u/kgomes/WaveGlider/security/2020-08 directory. I then ssh'd into that same directory and ran:

```bash
keytool -import -alias mbariwgmsclient -file wgms-com.pem -storepass wgmsclient -keystore mbari.wgms.keystore
```

Then, I edited the /u/kgomes/WaveGlider/waveglider.sh file and changed the truststore to point to /u/kgomes/WaveGlider/security/2020-08/mbari.wgms.keystore.  Then things started working again.
