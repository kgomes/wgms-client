package org.mbari.odss.waveglider.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.logging.Logger;

import org.mbari.model.Packet.Container;
import org.mbari.model.Packet.Location;
import org.mbari.model.Packet.Record;
import org.mbari.model.Packet.RecordDescription;
import org.mbari.model.Packet.RecordVariable;
import org.mbari.model.Packet.RecordVariable.DataType;
import org.mbari.model.Packet.Source;

import com.google.protobuf.ByteString;

import MBARItracking.PlatformMessage;
import MBARItracking.PlatformMessage.PlatformReport;

public class DataCollector {
	/**
	 * The logger
	 */
	private static final Logger logger = Logger.getLogger("DataCollector");

	/**
	 * The properties read from the configuration file
	 */
	private Properties configurationProperties = new Properties();

	/**
	 * The connection to the local H2 database
	 */
	private Connection conn = null;

	/**
	 * The default constructor
	 */
	public DataCollector(File configurationFile) {
		logger.config("DataCollector constructor with configuration file "
				+ configurationFile.getAbsolutePath());

		// Read in the properties from the specified file
		if (configurationFile != null && configurationFile.exists()) {
			// Read in the properties
			try {
				configurationProperties.load(new FileInputStream(
						configurationFile));
			} catch (FileNotFoundException e) {
				logger.severe("FileNotFoundException caught while "
						+ "trying to read from configuration file: "
						+ e.getMessage());
			} catch (IOException e) {
				logger.severe("IOException caught while trying to "
						+ "read from configuration file: " + e.getMessage());
			}
		} else {
			throw new IllegalArgumentException(
					"The file specified was not found");
		}

		// Validate the loaded properties
		validateProperties();

		// TODO kgomes - should capture the results of the validation and bail
		// out if there is a problem.
	}

	/**
	 * A method that looks at all the properties and runs some sort of
	 * validation on them
	 * 
	 * @return
	 */
	private boolean validateProperties() {
		// The boolean to return
		boolean propertiesValid = false;

		// TODO kgomes - need to check some of the properties to see if we
		// should go ahead

		// Return the result
		return propertiesValid;
	}

	/**
	 * This method uses the configuration file properties to construct and
	 * return an AMQPClient
	 * 
	 * @return
	 */
	private AMQPClient getAMQPClients(String propertyBaseName) {
		// The AMQPClient to return
		AMQPClient amqpClient = null;

		// Grab the servername
		String serverHostname = configurationProperties
				.getProperty(propertyBaseName + ".messaging.server.hostname");

		// Grab the port
		String serverPortString = configurationProperties
				.getProperty(propertyBaseName + ".messaging.server.port");

		// Try to convert to an integer
		Integer serverPort = 5672;
		try {
			serverPort = Integer.parseInt(serverPortString);
		} catch (NumberFormatException e) {
			logger.severe("Could not convert port specified in properties ("
					+ serverPortString + ") to an integer: " + e.getMessage());
		}

		// Grab the vhost
		String serverVhost = configurationProperties
				.getProperty(propertyBaseName + ".messaging.server.vhost");

		// Grab the username
		String serverUsername = configurationProperties
				.getProperty(propertyBaseName + ".messaging.server.username");

		// And the password
		String serverPassword = configurationProperties
				.getProperty(propertyBaseName + ".messaging.server.password");
		logger.config("Getting AMQP client with properties:\n" + "Host: "
				+ serverHostname + "\nPort: " + serverPort + "\nVHost: "
				+ serverVhost + "\nUsername: " + serverUsername
				+ "\nPassword: *********");

		// Create the client
		amqpClient = new AMQPClient(serverHostname, serverPort, serverVhost,
				serverUsername, serverPassword);

		// Return the client
		return amqpClient;
	}

	/**
	 * This method connects to the local database that is used to keep track of
	 * which messages have been published. It also creates the connection that
	 * will be used during this processing and makes sure the table that it
	 * needs exists.
	 */
	private void connectToDatabase() {
		try {
			Class.forName("org.h2.Driver");
		} catch (ClassNotFoundException e) {
			logger.severe("ClassNotFoundException caught trying to get H2 driver: "
					+ e.getMessage());
		}
		try {
			conn = DriverManager.getConnection("jdbc:h2:wgms_client", "sa", "");
		} catch (SQLException e) {
			logger.severe("SQLException trying to get a connection "
					+ "the the local wgms_client database: " + e.getMessage());
		}
		// Make sure the table exists
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS pub_messages "
					+ "(vehicle_id BIGINT, epochms BIGINT, gmt_time TIMESTAMP, type VARCHAR(50))");
			stmt.close();
		} catch (SQLException ex) {
			logger.severe("SQLException caught trying to create "
					+ "pub_messages table in the in memory DB: "
					+ ex.getMessage());
		}
	}

	/**
	 * This method searches the in-memory database to see if the message has
	 * been published already
	 * 
	 * @param vehicleId
	 * @param timestamp
	 * @param type
	 * @return
	 */
	private boolean checkIfMessagePublished(Integer vehicleId, Date timestamp,
			String type) {
		// The boolean to return
		boolean published = false;

		// Make sure parameters are sane
		if (vehicleId != null && timestamp != null && type != null
				&& !type.equals("")) {
			// Query for the message
			try {
				Statement stmt = conn.createStatement();
				stmt.execute("SELECT * FROM PUB_MESSAGES WHERE vehicle_id = "
						+ vehicleId + " AND epochms = " + timestamp.getTime()
						+ " AND type = '" + type + "'");
				// See if something was returned
				ResultSet rs = stmt.getResultSet();
				if (rs != null && rs.next())
					published = true;
				stmt.close();
			} catch (SQLException ex) {
				logger.severe("SQLException caught trying to look for record in "
						+ "pub_messages table in the in memory DB: "
						+ ex.getMessage());
			}
		}
		return published;
	}

	/**
	 * This method puts an entry in the local database that indicates that a
	 * particular message was published.
	 * 
	 * @param vehicleId
	 * @param timestamp
	 * @param type
	 */
	private void markMessagePublished(Integer vehicleId, Date timestamp,
			String type) {
		// Create a datformat for inserting date field
		DateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		sqlDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

		// Make sure parameters are sane
		if (vehicleId != null && timestamp != null && type != null
				&& !type.equals("")) {
			// Query for the message
			try {
				Statement stmt = conn.createStatement();
				stmt.execute("INSERT INTO PUB_MESSAGES VALUES (" + vehicleId
						+ ", " + timestamp.getTime() + ", '"
						+ sqlDateFormat.format(timestamp) + "', '" + type
						+ "')");
				stmt.close();
			} catch (SQLException ex) {
				logger.severe("SQLException caught trying to insert into "
						+ "pub_messages table in the in memory DB: "
						+ ex.getMessage());
			}
		}
	}

	/**
	 * This method closes the connection the local database
	 */
	private void cleanup() {
		// Close the connection
		try {
			conn.close();
		} catch (SQLException e) {
			logger.severe("SQLException caught trying to close the local datbase: "
					+ e.getMessage());
		}
	}

	/**
	 * This method takes in a WGMSClient and uses it to collect location data
	 * and publish that data to the AMPQ server (if it needs to be published)
	 * 
	 * @param wgmsClient
	 */
	private void collectAndPublishLocationData(WGMSClient wgmsClient) {
		logger.fine("collectAndPublishLocationData called");

		// Grab the number of vehicle positions to ask for
		Integer numberOfRecords = 10;
		logger.fine("Number of records from properties for locations is "
				+ configurationProperties.getProperty("vehicle."
						+ wgmsClient.getVehicleId()
						+ ".wgms.data.location.number.of.records"));
		if (configurationProperties.getProperty("vehicle."
				+ wgmsClient.getVehicleId()
				+ ".wgms.data.location.number.of.records") != null
				&& !configurationProperties.getProperty(
						"vehicle." + wgmsClient.getVehicleId()
								+ ".wgms.data.location.number.of.records")
						.equals("")) {
			// Convert to integer
			try {
				numberOfRecords = Integer.parseInt(configurationProperties
						.getProperty("vehicle." + wgmsClient.getVehicleId()
								+ ".wgms.data.location.number.of.records"));
			} catch (NumberFormatException e) {
				logger.severe("NumberFormatException trying to convert "
						+ configurationProperties.getProperty("vehicle."
								+ wgmsClient.getVehicleId()
								+ ".wgms.data.location.number.of.records")
						+ " to an integer: " + e.getMessage());
				logger.severe("Will default to 10 records");
			}
		}
		
		// Grab the data from the server
		TreeMap<Date, Double[]> locations = wgmsClient
				.readVehiclePosition(numberOfRecords);
		logger.fine(locations.size() + " locations returned");

		// Grab the amqp client associated with location data
		AMQPClient locationClient = getAMQPClients("vehicle."
				+ wgmsClient.getVehicleId() + ".wgms.data.location");

		// Connect to server
		locationClient.connectToServer();

		// Iterate over the returned locations
		for (Iterator<Date> iterator = locations.keySet().iterator(); iterator
				.hasNext();) {
			// Date format for string publishing
			DateFormat stringMessageDateFormat = new SimpleDateFormat(
					"yyyy-MM-dd'T'HH:mm:ss'Z'");
			stringMessageDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

			// Grab the data
			Date timestamp = iterator.next();
			logger.fine("Fix: " + stringMessageDateFormat.format(timestamp)
					+ ",Lat: " + locations.get(timestamp)[0] + ", Lon: "
					+ locations.get(timestamp)[1]);

			// Check to see if message was published yet
			boolean messagePublished = checkIfMessagePublished(
					wgmsClient.getVehicleId(), timestamp, "location");
			logger.fine("Published? " + messagePublished);

			// If not published, go ahead and publish it
			if (!messagePublished && configurationProperties.getProperty("publish.to.amqp").equals("true")) {
				// Grab the vehicle name from the properties
				String vehicleName = configurationProperties
						.getProperty("vehicle." + wgmsClient.getVehicleId()
								+ ".name");

				// First thing to do is read the list of exchanges that we are
				// supposed to publish to
				String[] exchangeList = configurationProperties
						.getProperty(
								"vehicle."
										+ wgmsClient.getVehicleId()
										+ ".wgms.data.location.messaging.server.exchanges")
						.split(",");

				// Make sure there were exchanges listed
				if (exchangeList != null && exchangeList.length > 0) {
					logger.fine("Will publish location to "
							+ exchangeList.length + " exchanges");
					// Loop over the list of exchanges
					for (int i = 0; i < exchangeList.length; i++) {
						String exchange = exchangeList[i];
						// Grab the format of the message from the properties
						String messageFormat = configurationProperties
								.getProperty("vehicle."
										+ wgmsClient.getVehicleId()
										+ ".wgms.data.location.exchange."
										+ exchange + ".format");
						logger.fine("Will publish to exchange " + exchange
								+ " in " + messageFormat + " format.");

						// Make sure it was read OK
						if (messageFormat != null) {
							// This will be the message to send
							byte[] messageBytes = null;

							// If string format
							if (messageFormat.equalsIgnoreCase("string")) {

								// Create the message in string form
								String messageString = "glider,"
										+ vehicleName
										+ ","
										+ timestamp.getTime()
										/ 1000
										+ ","
										+ locations.get(timestamp)[0]
										+ ","
										+ locations.get(timestamp)[1]
										+ ",wgms_client,,,"
										+ stringMessageDateFormat
												.format(timestamp);
								logger.fine("String message that will be sent to exchange "
										+ exchange + ":\n" + messageString);

								// Convert to payload bytes
								messageBytes = messageString.getBytes();
							} else if (messageFormat.equalsIgnoreCase("pb")) {
								// Build a new platform report from the protocol
								// buffers classes
								PlatformReport platformReport = PlatformReport
										.newBuilder()
										.setName(vehicleName)
										.setEpochSeconds(
												timestamp.getTime() / 1000)
										.setLatitude(
												locations.get(timestamp)[0])
										.setLongitude(
												locations.get(timestamp)[1])
										.setSource("wgms_client")
										.setIsoDatetime(
												stringMessageDateFormat
														.format(timestamp))
										.build();
								// Grab the bytes from the string serialization
								logger.fine("PlatformReport as string:\n"
										+ platformReport.toString());
								messageBytes = platformReport.toByteArray();
							} else {
								logger.severe("Format of "
										+ messageFormat
										+ " for exchange "
										+ exchange
										+ " is not recoginzed (valid options are 'ctd' or 'pb')");
							}

							// Check for payload and send if it is there
							if (messageBytes != null) {
								locationClient.publishMessageToFanoutExchange(
										messageBytes, exchange);
							}
						} else {
							logger.warning("No message format was found for location data to exchange "
									+ exchange
									+ " for vehicle "
									+ wgmsClient.getVehicleId());
						}
					}
				} else {
					logger.warning("No list of exchanges was found in the "
							+ "configuration properties for location data fors vehicle "
							+ wgmsClient.getVehicleId());
				}

				// Write that message was published
				markMessagePublished(wgmsClient.getVehicleId(), timestamp,
						"location");
			}
		}

		// Clean up the AMQP client connection
		locationClient.cleanup();
	}

	/**
	 * This method uses the supplied WGMSClient to request CTD records and then
	 * publishes them to AMQP if the have not been published yet.
	 * 
	 * @param wgmsClient
	 */
	private void collectAndPublishCTDData(WGMSClient wgmsClient) {
		logger.fine("collectAndPublishCTDData called");
		// Grab the number of vehicle positions to ask for
		Integer numberOfRecords = 10;
		if (configurationProperties.getProperty("vehicle."
				+ wgmsClient.getVehicleId()
				+ ".wgms.data.ctd.number.of.records") != null
				&& !configurationProperties.getProperty(
						"vehicle." + wgmsClient.getVehicleId()
								+ ".wgms.data.ctd.number.of.records")
						.equals("")) {
			// Convert to integer
			try {
				numberOfRecords = Integer.parseInt(configurationProperties
						.getProperty("vehicle." + wgmsClient.getVehicleId()
								+ ".wgms.data.ctd.number.of.records"));
			} catch (NumberFormatException e) {
				logger.severe("NumberFormatException trying to convert "
						+ configurationProperties.getProperty("vehicle."
								+ wgmsClient.getVehicleId()
								+ ".wgms.data.ctd.number.of.records")
						+ " to an integer: " + e.getMessage());
				logger.severe("Will default to 10 records");
			}
		}

		// Read the CTD records from the server
		TreeMap<Date, Double[]> ctdData = wgmsClient
				.readCTDData(numberOfRecords);
		logger.fine(ctdData.size() + " ctd records returned");

		// Build the source
		Source source = Source.newBuilder()
				.setIdentifier(wgmsClient.getVehicleId() + "")
				.setIdentifierNamespace("com.liquidr.platform.waveglider")
				.setDescription("Liquid Robotics Waveglider").build();

		// Build the RecordVariables
		RecordVariable conductivityRV = RecordVariable.newBuilder()
				.setRecordType(1).setColumnIndex(1).setName("Conductivity")
				.setLongName("Conductivity from wave glider")
				.setDataType(RecordVariable.DataType.DOUBLE).setUnits("S/m")
				.build();
		RecordVariable temperatureRV = RecordVariable.newBuilder()
				.setRecordType(1).setColumnIndex(2).setName("Temperature")
				.setLongName("Water temperature from wave glider")
				.setDataType(RecordVariable.DataType.DOUBLE).setUnits("C")
				.build();
		RecordVariable pressureRV = RecordVariable.newBuilder()
				.setRecordType(1).setColumnIndex(3).setName("Depth")
				.setLongName("Depth from wave glider")
				.setDataType(RecordVariable.DataType.DOUBLE).setUnits("m")
				.build();

		// Build the RecordDescription
		RecordDescription recordDescription = RecordDescription.newBuilder()
				.setRecordType(1).setRecordTypeName("CTD")
				.setBufferStyle(RecordDescription.BufferStyle.ASCII)
				.setBufferItemSeparator(",").setParseable(true)
				.addRecordVariables(conductivityRV)
				.addRecordVariables(temperatureRV)
				.addRecordVariables(pressureRV).build();

		// Grab the amqp client associated with location data
		AMQPClient ctdClient = getAMQPClients("vehicle."
				+ wgmsClient.getVehicleId() + ".wgms.data.ctd");

		// Grab the list of exchanges from the properties file and declare
		// exchanges on them
		String[] exchanges = ((String) configurationProperties.get("vehicle."
				+ wgmsClient.getVehicleId()
				+ ".wgms.data.ctd.messaging.server.exchanges")).split(",");

		logger.fine("Exchange list is " + exchanges);
		for (int i = 0; i < exchanges.length; i++) {
			ctdClient.declareTopicExchange(exchanges[0]);
		}

		// Connect to server
		ctdClient.connectToServer();

		// Iterate over the CTD records and publish if needed
		for (Iterator<Date> iterator = ctdData.keySet().iterator(); iterator
				.hasNext();) {
			Date timestamp = iterator.next();
			logger.fine("CTD Data" + timestamp + ",Pressure: "
					+ ctdData.get(timestamp)[2] + ", Temperature: "
					+ ctdData.get(timestamp)[3] + ", Conductivity: "
					+ ctdData.get(timestamp)[4]);

			// Check to see if message was published yet
			boolean messagePublished = checkIfMessagePublished(
					wgmsClient.getVehicleId(), timestamp, "ctd");
			logger.fine("Published? " + messagePublished);

			// Publish message if not published already
			if (!messagePublished) {
				// Create a longitude object first
				Location location = Location.newBuilder()
						.setEpochMilliseconds(timestamp.getTime())
						.setLatitude(ctdData.get(timestamp)[0])
						.setLongitude(ctdData.get(timestamp)[1]).build();

				// Build the record
				Record record = Record
						.newBuilder()
						.setEpochMilliseconds(timestamp.getTime())
						.setRecordType(1)
						.setLocation(location)
						.setPayload(
								ByteString.copyFrom((ctdData.get(timestamp)[2]
										+ "," + ctdData.get(timestamp)[3] + "," + ctdData
										.get(timestamp)[4]).getBytes()))
						.build();

				// Build a new container
				Container container = Container.newBuilder().setSource(source)
						.addRecordDescriptions(recordDescription)
						.addRecords(record).build();

				logger.fine("Container is " + container.toString());

				// Iterate over the exchanges
				for (int i = 0; i < exchanges.length; i++) {
					logger.fine("Going to publish to exchange: " + exchanges[i]);
					String routingKey = source.getIdentifierNamespace() + "."
							+ source.getIdentifier();
					logger.fine("Routing key will be " + routingKey);
					// Not going to worry about the format as it will always be
					// protocol buffers
					ctdClient.publishMessageToTopicExchange(
							container.toByteArray(), exchanges[i], routingKey);
				}

				// Write that message was published
				markMessagePublished(wgmsClient.getVehicleId(), timestamp,
						"ctd");
			}
		}

		// Clean up the connection to the messaging server
		ctdClient.cleanup();
	}

	/**
	 * This method uses the properties from the configuration file to read the
	 * data coming from the WGMS service and the publishes that data to the AMQP
	 * server.
	 */
	public void collectData() {
		// Get a connection to the local in-memory database so we can keep track
		// of which messages have been sent
		connectToDatabase();

		try {
			// To collect data from each of the vehicles identified in the
			// properties file, we need to grab the list of IDs first
			String[] vehicleIds = configurationProperties.getProperty(
					"vehicle.ids").split(",");
			logger.fine("Will get data for " + vehicleIds.length + " vehicles.");

			// Now iterate over each vehicle
			for (int i = 0; i < vehicleIds.length; i++) {
				logger.fine("Working with vehicle ID " + vehicleIds[i]);

				// Grab the properties for this vehicle
				String vehicleName = configurationProperties
						.getProperty("vehicle." + vehicleIds[i] + ".name");
				String vehicleEntityType = configurationProperties
						.getProperty("vehicle." + vehicleIds[i]
								+ ".entity.type");
				String vehicleRelatedEntity = configurationProperties
						.getProperty("vehicle." + vehicleIds[i]
								+ ".related.entity.type");
				String vehicleWgmsServerUrl = configurationProperties
						.getProperty("vehicle." + vehicleIds[i]
								+ ".wgms.server.url");
				String vehicleWgmsServerUsername = configurationProperties
						.getProperty("vehicle." + vehicleIds[i]
								+ ".wgms.server.username");
				String vehicleWgmsServerPassword = configurationProperties
						.getProperty("vehicle." + vehicleIds[i]
								+ ".wgms.server.password");
				String vehicleWgmsServerOrgName = configurationProperties
						.getProperty("vehicle." + vehicleIds[i]
								+ ".wgms.server.org.name");
				String[] vehicleWgmsData = configurationProperties.getProperty(
						"vehicle." + vehicleIds[i] + ".wgms.data").split(",");

				logger.fine("Properties are:\nVehicleName: "
						+ vehicleName
						+ "\nVehicleEntityType: "
						+ vehicleEntityType
						+ "\nVehicleRelatedEntity: "
						+ vehicleRelatedEntity
						+ "\nVehicleWgmsServerUrl: "
						+ vehicleWgmsServerUrl
						+ "\nVehicleWgmsServerUsername: "
						+ vehicleWgmsServerUsername
						+ "\nVehicleWgmsServerPassword: *******\nVehicleWgmsServerOrgName: "
						+ vehicleWgmsServerOrgName);
				for (int j = 0; j < vehicleWgmsData.length; j++) {
					logger.fine("Data: " + vehicleWgmsData[j]);
				}

				// Convert IDs to integers
				Integer vehicleId = null;
				Integer entityType = null;
				Integer relatedEntityType = null;
				try {
					vehicleId = Integer.parseInt(vehicleIds[i]);
					entityType = Integer.parseInt(vehicleEntityType);
					relatedEntityType = Integer.parseInt(vehicleRelatedEntity);
				} catch (NumberFormatException e) {
					logger.severe("Could not convert on of the IDs (entityType="
							+ vehicleEntityType
							+ ", relatedEntityType = "
							+ vehicleRelatedEntity
							+ ") of "
							+ vehicleIds[i]
							+ " to an integer: " + e.getMessage());
				}

				// Create the WGMSClient for this
				WGMSClient wgmsClient = new WGMSClient(vehicleWgmsServerUrl,
						vehicleWgmsServerUsername, vehicleWgmsServerPassword,
						vehicleWgmsServerOrgName, vehicleId, entityType,
						relatedEntityType);

				// Login the client in
				wgmsClient.loginToService();

				// Now loop over the data types to collect
				for (int j = 0; j < vehicleWgmsData.length; j++) {
					if (vehicleWgmsData[j].equalsIgnoreCase("location")) {
						collectAndPublishLocationData(wgmsClient);
					} else if (vehicleWgmsData[j].equalsIgnoreCase("ctd")) {
//						collectAndPublishCTDData(wgmsClient);
					}
				}
			}
		} catch (Exception e) {
			logger.severe("Exception caught trying to collect data from all vehicles: "
					+ e.getMessage());
		}
		// Clean up the database
		cleanup();
	}

	/**
	 * The main method that logs into the web service and extracts and publishes
	 * data to the AMQP bus
	 * 
	 * @param args
	 * @throws java.lang.Exception
	 */
	public static void main(String args[]) throws java.lang.Exception {
		// Check to makes sure configuration file was specified
		if (args.length < 1) {
			System.out
					.println("You must specify the configuration file that "
							+ "contains the properties that this client will use to run:");
		}

		// Grab the configuration file name from the command line
		String configurationFilePath = args[0];

		// Try to find as a file
		File configurationFile = new File(configurationFilePath);

		// Make sure it exists
		if (configurationFile.exists()) {
			// Create the DataCollector
			DataCollector dataCollector = new DataCollector(configurationFile);

			// Fire off the data collection
			dataCollector.collectData();
		} else {
			System.out.println("The file you specified ("
					+ configurationFilePath + ") does not seem to exist.");
		}
	}
}
