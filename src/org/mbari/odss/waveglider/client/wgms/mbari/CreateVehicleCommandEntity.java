
package org.mbari.odss.waveglider.client.wgms.mbari;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="packet" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="commandName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "packet",
    "commandName"
})
@XmlRootElement(name = "CreateVehicleCommandEntity")
public class CreateVehicleCommandEntity {

    protected byte[] packet;
    protected String commandName;

    /**
     * Gets the value of the packet property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getPacket() {
        return packet;
    }

    /**
     * Sets the value of the packet property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setPacket(byte[] value) {
        this.packet = value;
    }

    /**
     * Gets the value of the commandName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommandName() {
        return commandName;
    }

    /**
     * Sets the value of the commandName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommandName(String value) {
        this.commandName = value;
    }

}
