
package org.mbari.odss.waveglider.client.wgms.mbari;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateCommTimeoutAlarmResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateCommTimeoutAlarmResult"
})
@XmlRootElement(name = "UpdateCommTimeoutAlarmResponse")
public class UpdateCommTimeoutAlarmResponse {

    @XmlElement(name = "UpdateCommTimeoutAlarmResult")
    protected String updateCommTimeoutAlarmResult;

    /**
     * Gets the value of the updateCommTimeoutAlarmResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdateCommTimeoutAlarmResult() {
        return updateCommTimeoutAlarmResult;
    }

    /**
     * Sets the value of the updateCommTimeoutAlarmResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdateCommTimeoutAlarmResult(String value) {
        this.updateCommTimeoutAlarmResult = value;
    }

}
