
package org.mbari.odss.waveglider.client.wgms.mbari;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetEntitySetWithCriteriaJSONResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getEntitySetWithCriteriaJSONResult"
})
@XmlRootElement(name = "GetEntitySetWithCriteriaJSONResponse")
public class GetEntitySetWithCriteriaJSONResponse {

    @XmlElement(name = "GetEntitySetWithCriteriaJSONResult")
    protected String getEntitySetWithCriteriaJSONResult;

    /**
     * Gets the value of the getEntitySetWithCriteriaJSONResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetEntitySetWithCriteriaJSONResult() {
        return getEntitySetWithCriteriaJSONResult;
    }

    /**
     * Sets the value of the getEntitySetWithCriteriaJSONResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetEntitySetWithCriteriaJSONResult(String value) {
        this.getEntitySetWithCriteriaJSONResult = value;
    }

}
