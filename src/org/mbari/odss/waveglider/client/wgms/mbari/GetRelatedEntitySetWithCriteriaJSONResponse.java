
package org.mbari.odss.waveglider.client.wgms.mbari;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetRelatedEntitySetWithCriteriaJSONResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getRelatedEntitySetWithCriteriaJSONResult"
})
@XmlRootElement(name = "GetRelatedEntitySetWithCriteriaJSONResponse")
public class GetRelatedEntitySetWithCriteriaJSONResponse {

    @XmlElement(name = "GetRelatedEntitySetWithCriteriaJSONResult")
    protected String getRelatedEntitySetWithCriteriaJSONResult;

    /**
     * Gets the value of the getRelatedEntitySetWithCriteriaJSONResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetRelatedEntitySetWithCriteriaJSONResult() {
        return getRelatedEntitySetWithCriteriaJSONResult;
    }

    /**
     * Sets the value of the getRelatedEntitySetWithCriteriaJSONResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetRelatedEntitySetWithCriteriaJSONResult(String value) {
        this.getRelatedEntitySetWithCriteriaJSONResult = value;
    }

}
