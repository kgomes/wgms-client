
package org.mbari.odss.waveglider.client.wgms.mbari;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FixTelemetryForDatesResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fixTelemetryForDatesResult"
})
@XmlRootElement(name = "FixTelemetryForDatesResponse")
public class FixTelemetryForDatesResponse {

    @XmlElement(name = "FixTelemetryForDatesResult")
    protected String fixTelemetryForDatesResult;

    /**
     * Gets the value of the fixTelemetryForDatesResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFixTelemetryForDatesResult() {
        return fixTelemetryForDatesResult;
    }

    /**
     * Sets the value of the fixTelemetryForDatesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFixTelemetryForDatesResult(String value) {
        this.fixTelemetryForDatesResult = value;
    }

}
