
package org.mbari.odss.waveglider.client.wgms.vr2ctest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="commandName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vehicleId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="commandType" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="targetWaypointId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "commandName",
    "vehicleId",
    "commandType",
    "targetWaypointId"
})
@XmlRootElement(name = "MobileVehicleCommandEntity")
public class MobileVehicleCommandEntity {

    protected String commandName;
    protected int vehicleId;
    protected int commandType;
    protected int targetWaypointId;

    /**
     * Gets the value of the commandName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommandName() {
        return commandName;
    }

    /**
     * Sets the value of the commandName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommandName(String value) {
        this.commandName = value;
    }

    /**
     * Gets the value of the vehicleId property.
     * 
     */
    public int getVehicleId() {
        return vehicleId;
    }

    /**
     * Sets the value of the vehicleId property.
     * 
     */
    public void setVehicleId(int value) {
        this.vehicleId = value;
    }

    /**
     * Gets the value of the commandType property.
     * 
     */
    public int getCommandType() {
        return commandType;
    }

    /**
     * Sets the value of the commandType property.
     * 
     */
    public void setCommandType(int value) {
        this.commandType = value;
    }

    /**
     * Gets the value of the targetWaypointId property.
     * 
     */
    public int getTargetWaypointId() {
        return targetWaypointId;
    }

    /**
     * Sets the value of the targetWaypointId property.
     * 
     */
    public void setTargetWaypointId(int value) {
        this.targetWaypointId = value;
    }

}
