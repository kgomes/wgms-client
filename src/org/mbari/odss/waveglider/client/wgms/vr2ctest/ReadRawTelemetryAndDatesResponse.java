
package org.mbari.odss.waveglider.client.wgms.vr2ctest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReadRawTelemetryAndDatesResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "readRawTelemetryAndDatesResult"
})
@XmlRootElement(name = "ReadRawTelemetryAndDatesResponse")
public class ReadRawTelemetryAndDatesResponse {

    @XmlElement(name = "ReadRawTelemetryAndDatesResult")
    protected String readRawTelemetryAndDatesResult;

    /**
     * Gets the value of the readRawTelemetryAndDatesResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReadRawTelemetryAndDatesResult() {
        return readRawTelemetryAndDatesResult;
    }

    /**
     * Sets the value of the readRawTelemetryAndDatesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReadRawTelemetryAndDatesResult(String value) {
        this.readRawTelemetryAndDatesResult = value;
    }

}
