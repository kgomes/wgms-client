
package org.mbari.odss.waveglider.client.wgms.vr2ctest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="orgId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="vehicleId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="dateTimeFrom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dateTimeTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="swX" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="swY" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="neX" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="neY" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="rows" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "orgId",
    "vehicleId",
    "dateTimeFrom",
    "dateTimeTo",
    "swX",
    "swY",
    "neX",
    "neY",
    "rows"
})
@XmlRootElement(name = "GetVehicleCrumbData")
public class GetVehicleCrumbData {

    protected int orgId;
    protected int vehicleId;
    protected String dateTimeFrom;
    protected String dateTimeTo;
    protected double swX;
    protected double swY;
    protected double neX;
    protected double neY;
    protected int rows;

    /**
     * Gets the value of the orgId property.
     * 
     */
    public int getOrgId() {
        return orgId;
    }

    /**
     * Sets the value of the orgId property.
     * 
     */
    public void setOrgId(int value) {
        this.orgId = value;
    }

    /**
     * Gets the value of the vehicleId property.
     * 
     */
    public int getVehicleId() {
        return vehicleId;
    }

    /**
     * Sets the value of the vehicleId property.
     * 
     */
    public void setVehicleId(int value) {
        this.vehicleId = value;
    }

    /**
     * Gets the value of the dateTimeFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateTimeFrom() {
        return dateTimeFrom;
    }

    /**
     * Sets the value of the dateTimeFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateTimeFrom(String value) {
        this.dateTimeFrom = value;
    }

    /**
     * Gets the value of the dateTimeTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateTimeTo() {
        return dateTimeTo;
    }

    /**
     * Sets the value of the dateTimeTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateTimeTo(String value) {
        this.dateTimeTo = value;
    }

    /**
     * Gets the value of the swX property.
     * 
     */
    public double getSwX() {
        return swX;
    }

    /**
     * Sets the value of the swX property.
     * 
     */
    public void setSwX(double value) {
        this.swX = value;
    }

    /**
     * Gets the value of the swY property.
     * 
     */
    public double getSwY() {
        return swY;
    }

    /**
     * Sets the value of the swY property.
     * 
     */
    public void setSwY(double value) {
        this.swY = value;
    }

    /**
     * Gets the value of the neX property.
     * 
     */
    public double getNeX() {
        return neX;
    }

    /**
     * Sets the value of the neX property.
     * 
     */
    public void setNeX(double value) {
        this.neX = value;
    }

    /**
     * Gets the value of the neY property.
     * 
     */
    public double getNeY() {
        return neY;
    }

    /**
     * Sets the value of the neY property.
     * 
     */
    public void setNeY(double value) {
        this.neY = value;
    }

    /**
     * Gets the value of the rows property.
     * 
     */
    public int getRows() {
        return rows;
    }

    /**
     * Sets the value of the rows property.
     * 
     */
    public void setRows(int value) {
        this.rows = value;
    }

}
