
package org.mbari.odss.waveglider.client.wgms.vr2ctest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PurgeVehicleTelemetryResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "purgeVehicleTelemetryResult"
})
@XmlRootElement(name = "PurgeVehicleTelemetryResponse")
public class PurgeVehicleTelemetryResponse {

    @XmlElement(name = "PurgeVehicleTelemetryResult")
    protected String purgeVehicleTelemetryResult;

    /**
     * Gets the value of the purgeVehicleTelemetryResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurgeVehicleTelemetryResult() {
        return purgeVehicleTelemetryResult;
    }

    /**
     * Sets the value of the purgeVehicleTelemetryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurgeVehicleTelemetryResult(String value) {
        this.purgeVehicleTelemetryResult = value;
    }

}
