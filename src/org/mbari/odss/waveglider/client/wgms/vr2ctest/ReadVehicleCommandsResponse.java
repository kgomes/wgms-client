
package org.mbari.odss.waveglider.client.wgms.vr2ctest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReadVehicleCommandsResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "readVehicleCommandsResult"
})
@XmlRootElement(name = "ReadVehicleCommandsResponse")
public class ReadVehicleCommandsResponse {

    @XmlElement(name = "ReadVehicleCommandsResult")
    protected String readVehicleCommandsResult;

    /**
     * Gets the value of the readVehicleCommandsResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReadVehicleCommandsResult() {
        return readVehicleCommandsResult;
    }

    /**
     * Sets the value of the readVehicleCommandsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReadVehicleCommandsResult(String value) {
        this.readVehicleCommandsResult = value;
    }

}
