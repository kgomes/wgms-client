
package org.mbari.odss.waveglider.client.wgms.vr2ctest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetEntityColumnSelectMetadataSetResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getEntityColumnSelectMetadataSetResult"
})
@XmlRootElement(name = "GetEntityColumnSelectMetadataSetResponse")
public class GetEntityColumnSelectMetadataSetResponse {

    @XmlElement(name = "GetEntityColumnSelectMetadataSetResult")
    protected String getEntityColumnSelectMetadataSetResult;

    /**
     * Gets the value of the getEntityColumnSelectMetadataSetResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetEntityColumnSelectMetadataSetResult() {
        return getEntityColumnSelectMetadataSetResult;
    }

    /**
     * Sets the value of the getEntityColumnSelectMetadataSetResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetEntityColumnSelectMetadataSetResult(String value) {
        this.getEntityColumnSelectMetadataSetResult = value;
    }

}
