
package org.mbari.odss.waveglider.client.wgms.vr2ctest;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.mbari.odss.waveglider.client.wgms.vr2ctest package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.mbari.odss.waveglider.client.wgms.vr2ctest
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetEntitySet }
     * 
     */
    public GetEntitySet createGetEntitySet() {
        return new GetEntitySet();
    }

    /**
     * Create an instance of {@link GetEntitySetResponse }
     * 
     */
    public GetEntitySetResponse createGetEntitySetResponse() {
        return new GetEntitySetResponse();
    }

    /**
     * Create an instance of {@link GetEntitySetWithCriteria }
     * 
     */
    public GetEntitySetWithCriteria createGetEntitySetWithCriteria() {
        return new GetEntitySetWithCriteria();
    }

    /**
     * Create an instance of {@link GetEntitySetWithCriteriaResponse }
     * 
     */
    public GetEntitySetWithCriteriaResponse createGetEntitySetWithCriteriaResponse() {
        return new GetEntitySetWithCriteriaResponse();
    }

    /**
     * Create an instance of {@link GetRelatedEntitySet }
     * 
     */
    public GetRelatedEntitySet createGetRelatedEntitySet() {
        return new GetRelatedEntitySet();
    }

    /**
     * Create an instance of {@link GetRelatedEntitySetResponse }
     * 
     */
    public GetRelatedEntitySetResponse createGetRelatedEntitySetResponse() {
        return new GetRelatedEntitySetResponse();
    }

    /**
     * Create an instance of {@link GetRelatedEntitySetWithCriteria }
     * 
     */
    public GetRelatedEntitySetWithCriteria createGetRelatedEntitySetWithCriteria() {
        return new GetRelatedEntitySetWithCriteria();
    }

    /**
     * Create an instance of {@link GetRelatedEntitySetWithCriteriaResponse }
     * 
     */
    public GetRelatedEntitySetWithCriteriaResponse createGetRelatedEntitySetWithCriteriaResponse() {
        return new GetRelatedEntitySetWithCriteriaResponse();
    }

    /**
     * Create an instance of {@link GetEntitySetJSON }
     * 
     */
    public GetEntitySetJSON createGetEntitySetJSON() {
        return new GetEntitySetJSON();
    }

    /**
     * Create an instance of {@link GetEntitySetJSONResponse }
     * 
     */
    public GetEntitySetJSONResponse createGetEntitySetJSONResponse() {
        return new GetEntitySetJSONResponse();
    }

    /**
     * Create an instance of {@link GetEntitySetWithCriteriaJSON }
     * 
     */
    public GetEntitySetWithCriteriaJSON createGetEntitySetWithCriteriaJSON() {
        return new GetEntitySetWithCriteriaJSON();
    }

    /**
     * Create an instance of {@link GetEntitySetWithCriteriaJSONResponse }
     * 
     */
    public GetEntitySetWithCriteriaJSONResponse createGetEntitySetWithCriteriaJSONResponse() {
        return new GetEntitySetWithCriteriaJSONResponse();
    }

    /**
     * Create an instance of {@link GetRelatedEntitySetJSON }
     * 
     */
    public GetRelatedEntitySetJSON createGetRelatedEntitySetJSON() {
        return new GetRelatedEntitySetJSON();
    }

    /**
     * Create an instance of {@link GetRelatedEntitySetJSONResponse }
     * 
     */
    public GetRelatedEntitySetJSONResponse createGetRelatedEntitySetJSONResponse() {
        return new GetRelatedEntitySetJSONResponse();
    }

    /**
     * Create an instance of {@link GetRelatedEntitySetWithCriteriaJSON }
     * 
     */
    public GetRelatedEntitySetWithCriteriaJSON createGetRelatedEntitySetWithCriteriaJSON() {
        return new GetRelatedEntitySetWithCriteriaJSON();
    }

    /**
     * Create an instance of {@link GetRelatedEntitySetWithCriteriaJSONResponse }
     * 
     */
    public GetRelatedEntitySetWithCriteriaJSONResponse createGetRelatedEntitySetWithCriteriaJSONResponse() {
        return new GetRelatedEntitySetWithCriteriaJSONResponse();
    }

    /**
     * Create an instance of {@link GetFormFrameworkControl }
     * 
     */
    public GetFormFrameworkControl createGetFormFrameworkControl() {
        return new GetFormFrameworkControl();
    }

    /**
     * Create an instance of {@link GetFormFrameworkControlResponse }
     * 
     */
    public GetFormFrameworkControlResponse createGetFormFrameworkControlResponse() {
        return new GetFormFrameworkControlResponse();
    }

    /**
     * Create an instance of {@link CreateVehicleOutputEntity }
     * 
     */
    public CreateVehicleOutputEntity createCreateVehicleOutputEntity() {
        return new CreateVehicleOutputEntity();
    }

    /**
     * Create an instance of {@link CreateVehicleOutputEntityResponse }
     * 
     */
    public CreateVehicleOutputEntityResponse createCreateVehicleOutputEntityResponse() {
        return new CreateVehicleOutputEntityResponse();
    }

    /**
     * Create an instance of {@link CreateVehicleCommandEntity }
     * 
     */
    public CreateVehicleCommandEntity createCreateVehicleCommandEntity() {
        return new CreateVehicleCommandEntity();
    }

    /**
     * Create an instance of {@link CreateVehicleCommandEntityResponse }
     * 
     */
    public CreateVehicleCommandEntityResponse createCreateVehicleCommandEntityResponse() {
        return new CreateVehicleCommandEntityResponse();
    }

    /**
     * Create an instance of {@link CreateVehicleConfigurationEntity }
     * 
     */
    public CreateVehicleConfigurationEntity createCreateVehicleConfigurationEntity() {
        return new CreateVehicleConfigurationEntity();
    }

    /**
     * Create an instance of {@link CreateVehicleConfigurationEntityResponse }
     * 
     */
    public CreateVehicleConfigurationEntityResponse createCreateVehicleConfigurationEntityResponse() {
        return new CreateVehicleConfigurationEntityResponse();
    }

    /**
     * Create an instance of {@link ReadVehicleCommandEntity }
     * 
     */
    public ReadVehicleCommandEntity createReadVehicleCommandEntity() {
        return new ReadVehicleCommandEntity();
    }

    /**
     * Create an instance of {@link ReadVehicleCommandEntityResponse }
     * 
     */
    public ReadVehicleCommandEntityResponse createReadVehicleCommandEntityResponse() {
        return new ReadVehicleCommandEntityResponse();
    }

    /**
     * Create an instance of {@link ReadVehicleCommands }
     * 
     */
    public ReadVehicleCommands createReadVehicleCommands() {
        return new ReadVehicleCommands();
    }

    /**
     * Create an instance of {@link ReadVehicleCommandsResponse }
     * 
     */
    public ReadVehicleCommandsResponse createReadVehicleCommandsResponse() {
        return new ReadVehicleCommandsResponse();
    }

    /**
     * Create an instance of {@link SetVehicleCommandSent }
     * 
     */
    public SetVehicleCommandSent createSetVehicleCommandSent() {
        return new SetVehicleCommandSent();
    }

    /**
     * Create an instance of {@link SetVehicleCommandSentResponse }
     * 
     */
    public SetVehicleCommandSentResponse createSetVehicleCommandSentResponse() {
        return new SetVehicleCommandSentResponse();
    }

    /**
     * Create an instance of {@link ReadVehicleConfigurationEntity }
     * 
     */
    public ReadVehicleConfigurationEntity createReadVehicleConfigurationEntity() {
        return new ReadVehicleConfigurationEntity();
    }

    /**
     * Create an instance of {@link ReadVehicleConfigurationEntityResponse }
     * 
     */
    public ReadVehicleConfigurationEntityResponse createReadVehicleConfigurationEntityResponse() {
        return new ReadVehicleConfigurationEntityResponse();
    }

    /**
     * Create an instance of {@link UpdateCommTimeoutAlarm }
     * 
     */
    public UpdateCommTimeoutAlarm createUpdateCommTimeoutAlarm() {
        return new UpdateCommTimeoutAlarm();
    }

    /**
     * Create an instance of {@link UpdateCommTimeoutAlarmResponse }
     * 
     */
    public UpdateCommTimeoutAlarmResponse createUpdateCommTimeoutAlarmResponse() {
        return new UpdateCommTimeoutAlarmResponse();
    }

    /**
     * Create an instance of {@link CancelAlarm }
     * 
     */
    public CancelAlarm createCancelAlarm() {
        return new CancelAlarm();
    }

    /**
     * Create an instance of {@link CancelAlarmResponse }
     * 
     */
    public CancelAlarmResponse createCancelAlarmResponse() {
        return new CancelAlarmResponse();
    }

    /**
     * Create an instance of {@link AlarmStatus }
     * 
     */
    public AlarmStatus createAlarmStatus() {
        return new AlarmStatus();
    }

    /**
     * Create an instance of {@link AlarmStatusResponse }
     * 
     */
    public AlarmStatusResponse createAlarmStatusResponse() {
        return new AlarmStatusResponse();
    }

    /**
     * Create an instance of {@link MobileStatus }
     * 
     */
    public MobileStatus createMobileStatus() {
        return new MobileStatus();
    }

    /**
     * Create an instance of {@link MobileStatusResponse }
     * 
     */
    public MobileStatusResponse createMobileStatusResponse() {
        return new MobileStatusResponse();
    }

    /**
     * Create an instance of {@link ReadRawTelemetryAndDates }
     * 
     */
    public ReadRawTelemetryAndDates createReadRawTelemetryAndDates() {
        return new ReadRawTelemetryAndDates();
    }

    /**
     * Create an instance of {@link ReadRawTelemetryAndDatesResponse }
     * 
     */
    public ReadRawTelemetryAndDatesResponse createReadRawTelemetryAndDatesResponse() {
        return new ReadRawTelemetryAndDatesResponse();
    }

    /**
     * Create an instance of {@link ProcessNMEASerialPort }
     * 
     */
    public ProcessNMEASerialPort createProcessNMEASerialPort() {
        return new ProcessNMEASerialPort();
    }

    /**
     * Create an instance of {@link ProcessNMEASerialPortResponse }
     * 
     */
    public ProcessNMEASerialPortResponse createProcessNMEASerialPortResponse() {
        return new ProcessNMEASerialPortResponse();
    }

    /**
     * Create an instance of {@link FixTelemetryForDates }
     * 
     */
    public FixTelemetryForDates createFixTelemetryForDates() {
        return new FixTelemetryForDates();
    }

    /**
     * Create an instance of {@link FixTelemetryForDatesResponse }
     * 
     */
    public FixTelemetryForDatesResponse createFixTelemetryForDatesResponse() {
        return new FixTelemetryForDatesResponse();
    }

    /**
     * Create an instance of {@link UpdateWeatherUndergroundData }
     * 
     */
    public UpdateWeatherUndergroundData createUpdateWeatherUndergroundData() {
        return new UpdateWeatherUndergroundData();
    }

    /**
     * Create an instance of {@link UpdateWeatherUndergroundDataResponse }
     * 
     */
    public UpdateWeatherUndergroundDataResponse createUpdateWeatherUndergroundDataResponse() {
        return new UpdateWeatherUndergroundDataResponse();
    }

    /**
     * Create an instance of {@link MobileVehicleCommandEntity }
     * 
     */
    public MobileVehicleCommandEntity createMobileVehicleCommandEntity() {
        return new MobileVehicleCommandEntity();
    }

    /**
     * Create an instance of {@link MobileVehicleCommandEntityResponse }
     * 
     */
    public MobileVehicleCommandEntityResponse createMobileVehicleCommandEntityResponse() {
        return new MobileVehicleCommandEntityResponse();
    }

    /**
     * Create an instance of {@link GetEntityByID }
     * 
     */
    public GetEntityByID createGetEntityByID() {
        return new GetEntityByID();
    }

    /**
     * Create an instance of {@link GetEntityByIDResponse }
     * 
     */
    public GetEntityByIDResponse createGetEntityByIDResponse() {
        return new GetEntityByIDResponse();
    }

    /**
     * Create an instance of {@link GetVehicleForIMEI }
     * 
     */
    public GetVehicleForIMEI createGetVehicleForIMEI() {
        return new GetVehicleForIMEI();
    }

    /**
     * Create an instance of {@link GetVehicleForIMEIResponse }
     * 
     */
    public GetVehicleForIMEIResponse createGetVehicleForIMEIResponse() {
        return new GetVehicleForIMEIResponse();
    }

    /**
     * Create an instance of {@link GetVehicleForUniqueId }
     * 
     */
    public GetVehicleForUniqueId createGetVehicleForUniqueId() {
        return new GetVehicleForUniqueId();
    }

    /**
     * Create an instance of {@link GetVehicleForUniqueIdResponse }
     * 
     */
    public GetVehicleForUniqueIdResponse createGetVehicleForUniqueIdResponse() {
        return new GetVehicleForUniqueIdResponse();
    }

    /**
     * Create an instance of {@link GetServiceForId }
     * 
     */
    public GetServiceForId createGetServiceForId() {
        return new GetServiceForId();
    }

    /**
     * Create an instance of {@link GetServiceForIdResponse }
     * 
     */
    public GetServiceForIdResponse createGetServiceForIdResponse() {
        return new GetServiceForIdResponse();
    }

    /**
     * Create an instance of {@link GetVehicleRelatedCommunicationChannel }
     * 
     */
    public GetVehicleRelatedCommunicationChannel createGetVehicleRelatedCommunicationChannel() {
        return new GetVehicleRelatedCommunicationChannel();
    }

    /**
     * Create an instance of {@link GetVehicleRelatedCommunicationChannelResponse }
     * 
     */
    public GetVehicleRelatedCommunicationChannelResponse createGetVehicleRelatedCommunicationChannelResponse() {
        return new GetVehicleRelatedCommunicationChannelResponse();
    }

    /**
     * Create an instance of {@link GetSessionKey }
     * 
     */
    public GetSessionKey createGetSessionKey() {
        return new GetSessionKey();
    }

    /**
     * Create an instance of {@link GetSessionKeyResponse }
     * 
     */
    public GetSessionKeyResponse createGetSessionKeyResponse() {
        return new GetSessionKeyResponse();
    }

    /**
     * Create an instance of {@link ReplaceSessionKey }
     * 
     */
    public ReplaceSessionKey createReplaceSessionKey() {
        return new ReplaceSessionKey();
    }

    /**
     * Create an instance of {@link ReplaceSessionKeyResponse }
     * 
     */
    public ReplaceSessionKeyResponse createReplaceSessionKeyResponse() {
        return new ReplaceSessionKeyResponse();
    }

    /**
     * Create an instance of {@link IsSMCBasedIridium }
     * 
     */
    public IsSMCBasedIridium createIsSMCBasedIridium() {
        return new IsSMCBasedIridium();
    }

    /**
     * Create an instance of {@link IsSMCBasedIridiumResponse }
     * 
     */
    public IsSMCBasedIridiumResponse createIsSMCBasedIridiumResponse() {
        return new IsSMCBasedIridiumResponse();
    }

    /**
     * Create an instance of {@link GetMobileWGMSIdForVehicle }
     * 
     */
    public GetMobileWGMSIdForVehicle createGetMobileWGMSIdForVehicle() {
        return new GetMobileWGMSIdForVehicle();
    }

    /**
     * Create an instance of {@link GetMobileWGMSIdForVehicleResponse }
     * 
     */
    public GetMobileWGMSIdForVehicleResponse createGetMobileWGMSIdForVehicleResponse() {
        return new GetMobileWGMSIdForVehicleResponse();
    }

    /**
     * Create an instance of {@link GetMobileWGMSDispositionForVehicle }
     * 
     */
    public GetMobileWGMSDispositionForVehicle createGetMobileWGMSDispositionForVehicle() {
        return new GetMobileWGMSDispositionForVehicle();
    }

    /**
     * Create an instance of {@link GetMobileWGMSDispositionForVehicleResponse }
     * 
     */
    public GetMobileWGMSDispositionForVehicleResponse createGetMobileWGMSDispositionForVehicleResponse() {
        return new GetMobileWGMSDispositionForVehicleResponse();
    }

    /**
     * Create an instance of {@link GetVehicleAlarms }
     * 
     */
    public GetVehicleAlarms createGetVehicleAlarms() {
        return new GetVehicleAlarms();
    }

    /**
     * Create an instance of {@link GetVehicleAlarmsResponse }
     * 
     */
    public GetVehicleAlarmsResponse createGetVehicleAlarmsResponse() {
        return new GetVehicleAlarmsResponse();
    }

    /**
     * Create an instance of {@link GenerateAlarm }
     * 
     */
    public GenerateAlarm createGenerateAlarm() {
        return new GenerateAlarm();
    }

    /**
     * Create an instance of {@link GenerateAlarmResponse }
     * 
     */
    public GenerateAlarmResponse createGenerateAlarmResponse() {
        return new GenerateAlarmResponse();
    }

    /**
     * Create an instance of {@link ProcessAlarmEscallation }
     * 
     */
    public ProcessAlarmEscallation createProcessAlarmEscallation() {
        return new ProcessAlarmEscallation();
    }

    /**
     * Create an instance of {@link ProcessAlarmEscallationResponse }
     * 
     */
    public ProcessAlarmEscallationResponse createProcessAlarmEscallationResponse() {
        return new ProcessAlarmEscallationResponse();
    }

    /**
     * Create an instance of {@link CreateEntity }
     * 
     */
    public CreateEntity createCreateEntity() {
        return new CreateEntity();
    }

    /**
     * Create an instance of {@link CreateEntityResponse }
     * 
     */
    public CreateEntityResponse createCreateEntityResponse() {
        return new CreateEntityResponse();
    }

    /**
     * Create an instance of {@link ReadEntity }
     * 
     */
    public ReadEntity createReadEntity() {
        return new ReadEntity();
    }

    /**
     * Create an instance of {@link ReadEntityResponse }
     * 
     */
    public ReadEntityResponse createReadEntityResponse() {
        return new ReadEntityResponse();
    }

    /**
     * Create an instance of {@link UpdateEntity }
     * 
     */
    public UpdateEntity createUpdateEntity() {
        return new UpdateEntity();
    }

    /**
     * Create an instance of {@link UpdateEntityResponse }
     * 
     */
    public UpdateEntityResponse createUpdateEntityResponse() {
        return new UpdateEntityResponse();
    }

    /**
     * Create an instance of {@link XOUpdateEntity }
     * 
     */
    public XOUpdateEntity createXOUpdateEntity() {
        return new XOUpdateEntity();
    }

    /**
     * Create an instance of {@link XOUpdateEntityResponse }
     * 
     */
    public XOUpdateEntityResponse createXOUpdateEntityResponse() {
        return new XOUpdateEntityResponse();
    }

    /**
     * Create an instance of {@link DeleteEntity }
     * 
     */
    public DeleteEntity createDeleteEntity() {
        return new DeleteEntity();
    }

    /**
     * Create an instance of {@link DeleteEntityResponse }
     * 
     */
    public DeleteEntityResponse createDeleteEntityResponse() {
        return new DeleteEntityResponse();
    }

    /**
     * Create an instance of {@link EntityOrderUp }
     * 
     */
    public EntityOrderUp createEntityOrderUp() {
        return new EntityOrderUp();
    }

    /**
     * Create an instance of {@link EntityOrderUpResponse }
     * 
     */
    public EntityOrderUpResponse createEntityOrderUpResponse() {
        return new EntityOrderUpResponse();
    }

    /**
     * Create an instance of {@link EntityOrderDown }
     * 
     */
    public EntityOrderDown createEntityOrderDown() {
        return new EntityOrderDown();
    }

    /**
     * Create an instance of {@link EntityOrderDownResponse }
     * 
     */
    public EntityOrderDownResponse createEntityOrderDownResponse() {
        return new EntityOrderDownResponse();
    }

    /**
     * Create an instance of {@link CurrentUserId }
     * 
     */
    public CurrentUserId createCurrentUserId() {
        return new CurrentUserId();
    }

    /**
     * Create an instance of {@link CurrentUserIdResponse }
     * 
     */
    public CurrentUserIdResponse createCurrentUserIdResponse() {
        return new CurrentUserIdResponse();
    }

    /**
     * Create an instance of {@link CurrentUserIsRoot }
     * 
     */
    public CurrentUserIsRoot createCurrentUserIsRoot() {
        return new CurrentUserIsRoot();
    }

    /**
     * Create an instance of {@link CurrentUserIsRootResponse }
     * 
     */
    public CurrentUserIsRootResponse createCurrentUserIsRootResponse() {
        return new CurrentUserIsRootResponse();
    }

    /**
     * Create an instance of {@link GetVehiclesOnMission }
     * 
     */
    public GetVehiclesOnMission createGetVehiclesOnMission() {
        return new GetVehiclesOnMission();
    }

    /**
     * Create an instance of {@link GetVehiclesOnMissionResponse }
     * 
     */
    public GetVehiclesOnMissionResponse createGetVehiclesOnMissionResponse() {
        return new GetVehiclesOnMissionResponse();
    }

    /**
     * Create an instance of {@link PurgeVehicleTelemetry }
     * 
     */
    public PurgeVehicleTelemetry createPurgeVehicleTelemetry() {
        return new PurgeVehicleTelemetry();
    }

    /**
     * Create an instance of {@link PurgeVehicleTelemetryResponse }
     * 
     */
    public PurgeVehicleTelemetryResponse createPurgeVehicleTelemetryResponse() {
        return new PurgeVehicleTelemetryResponse();
    }

    /**
     * Create an instance of {@link GetVehicleCrumbData }
     * 
     */
    public GetVehicleCrumbData createGetVehicleCrumbData() {
        return new GetVehicleCrumbData();
    }

    /**
     * Create an instance of {@link GetVehicleCrumbDataResponse }
     * 
     */
    public GetVehicleCrumbDataResponse createGetVehicleCrumbDataResponse() {
        return new GetVehicleCrumbDataResponse();
    }

    /**
     * Create an instance of {@link GetAISData }
     * 
     */
    public GetAISData createGetAISData() {
        return new GetAISData();
    }

    /**
     * Create an instance of {@link GetAISDataResponse }
     * 
     */
    public GetAISDataResponse createGetAISDataResponse() {
        return new GetAISDataResponse();
    }

    /**
     * Create an instance of {@link Subscribe }
     * 
     */
    public Subscribe createSubscribe() {
        return new Subscribe();
    }

    /**
     * Create an instance of {@link SubscribeResponse }
     * 
     */
    public SubscribeResponse createSubscribeResponse() {
        return new SubscribeResponse();
    }

    /**
     * Create an instance of {@link SubscribeRelated }
     * 
     */
    public SubscribeRelated createSubscribeRelated() {
        return new SubscribeRelated();
    }

    /**
     * Create an instance of {@link SubscribeRelatedResponse }
     * 
     */
    public SubscribeRelatedResponse createSubscribeRelatedResponse() {
        return new SubscribeRelatedResponse();
    }

    /**
     * Create an instance of {@link UnSubscribe }
     * 
     */
    public UnSubscribe createUnSubscribe() {
        return new UnSubscribe();
    }

    /**
     * Create an instance of {@link UnSubscribeResponse }
     * 
     */
    public UnSubscribeResponse createUnSubscribeResponse() {
        return new UnSubscribeResponse();
    }

    /**
     * Create an instance of {@link WorkflowEventStartTransaction }
     * 
     */
    public WorkflowEventStartTransaction createWorkflowEventStartTransaction() {
        return new WorkflowEventStartTransaction();
    }

    /**
     * Create an instance of {@link WorkflowEventStartTransactionResponse }
     * 
     */
    public WorkflowEventStartTransactionResponse createWorkflowEventStartTransactionResponse() {
        return new WorkflowEventStartTransactionResponse();
    }

    /**
     * Create an instance of {@link WorkflowEventEndTransaction }
     * 
     */
    public WorkflowEventEndTransaction createWorkflowEventEndTransaction() {
        return new WorkflowEventEndTransaction();
    }

    /**
     * Create an instance of {@link WorkflowEventEndTransactionResponse }
     * 
     */
    public WorkflowEventEndTransactionResponse createWorkflowEventEndTransactionResponse() {
        return new WorkflowEventEndTransactionResponse();
    }

    /**
     * Create an instance of {@link GetEntityMetadata }
     * 
     */
    public GetEntityMetadata createGetEntityMetadata() {
        return new GetEntityMetadata();
    }

    /**
     * Create an instance of {@link GetEntityMetadataResponse }
     * 
     */
    public GetEntityMetadataResponse createGetEntityMetadataResponse() {
        return new GetEntityMetadataResponse();
    }

    /**
     * Create an instance of {@link GetEntityColumnMetadataSet }
     * 
     */
    public GetEntityColumnMetadataSet createGetEntityColumnMetadataSet() {
        return new GetEntityColumnMetadataSet();
    }

    /**
     * Create an instance of {@link GetEntityColumnMetadataSetResponse }
     * 
     */
    public GetEntityColumnMetadataSetResponse createGetEntityColumnMetadataSetResponse() {
        return new GetEntityColumnMetadataSetResponse();
    }

    /**
     * Create an instance of {@link GetEntityColumnMetadata }
     * 
     */
    public GetEntityColumnMetadata createGetEntityColumnMetadata() {
        return new GetEntityColumnMetadata();
    }

    /**
     * Create an instance of {@link GetEntityColumnMetadataResponse }
     * 
     */
    public GetEntityColumnMetadataResponse createGetEntityColumnMetadataResponse() {
        return new GetEntityColumnMetadataResponse();
    }

    /**
     * Create an instance of {@link GetEntityColumnSelectMetadata }
     * 
     */
    public GetEntityColumnSelectMetadata createGetEntityColumnSelectMetadata() {
        return new GetEntityColumnSelectMetadata();
    }

    /**
     * Create an instance of {@link GetEntityColumnSelectMetadataResponse }
     * 
     */
    public GetEntityColumnSelectMetadataResponse createGetEntityColumnSelectMetadataResponse() {
        return new GetEntityColumnSelectMetadataResponse();
    }

    /**
     * Create an instance of {@link GetEntityColumnSelectMetadataSet }
     * 
     */
    public GetEntityColumnSelectMetadataSet createGetEntityColumnSelectMetadataSet() {
        return new GetEntityColumnSelectMetadataSet();
    }

    /**
     * Create an instance of {@link GetEntityColumnSelectMetadataSetResponse }
     * 
     */
    public GetEntityColumnSelectMetadataSetResponse createGetEntityColumnSelectMetadataSetResponse() {
        return new GetEntityColumnSelectMetadataSetResponse();
    }

    /**
     * Create an instance of {@link GetSensorDataViews }
     * 
     */
    public GetSensorDataViews createGetSensorDataViews() {
        return new GetSensorDataViews();
    }

    /**
     * Create an instance of {@link GetSensorDataViewsResponse }
     * 
     */
    public GetSensorDataViewsResponse createGetSensorDataViewsResponse() {
        return new GetSensorDataViewsResponse();
    }

    /**
     * Create an instance of {@link GetVehicleRawRecords }
     * 
     */
    public GetVehicleRawRecords createGetVehicleRawRecords() {
        return new GetVehicleRawRecords();
    }

    /**
     * Create an instance of {@link GetVehicleRawRecordsResponse }
     * 
     */
    public GetVehicleRawRecordsResponse createGetVehicleRawRecordsResponse() {
        return new GetVehicleRawRecordsResponse();
    }

    /**
     * Create an instance of {@link GetVehicleCrumbs }
     * 
     */
    public GetVehicleCrumbs createGetVehicleCrumbs() {
        return new GetVehicleCrumbs();
    }

    /**
     * Create an instance of {@link GetVehicleCrumbsResponse }
     * 
     */
    public GetVehicleCrumbsResponse createGetVehicleCrumbsResponse() {
        return new GetVehicleCrumbsResponse();
    }

    /**
     * Create an instance of {@link GetVehicleCommands }
     * 
     */
    public GetVehicleCommands createGetVehicleCommands() {
        return new GetVehicleCommands();
    }

    /**
     * Create an instance of {@link GetVehicleCommandsResponse }
     * 
     */
    public GetVehicleCommandsResponse createGetVehicleCommandsResponse() {
        return new GetVehicleCommandsResponse();
    }

    /**
     * Create an instance of {@link GetVehiclePowerCtlStatuses }
     * 
     */
    public GetVehiclePowerCtlStatuses createGetVehiclePowerCtlStatuses() {
        return new GetVehiclePowerCtlStatuses();
    }

    /**
     * Create an instance of {@link GetVehiclePowerCtlStatusesResponse }
     * 
     */
    public GetVehiclePowerCtlStatusesResponse createGetVehiclePowerCtlStatusesResponse() {
        return new GetVehiclePowerCtlStatusesResponse();
    }

    /**
     * Create an instance of {@link VehicleSetParameterTitle }
     * 
     */
    public VehicleSetParameterTitle createVehicleSetParameterTitle() {
        return new VehicleSetParameterTitle();
    }

    /**
     * Create an instance of {@link VehicleSetParameterTitleResponse }
     * 
     */
    public VehicleSetParameterTitleResponse createVehicleSetParameterTitleResponse() {
        return new VehicleSetParameterTitleResponse();
    }

    /**
     * Create an instance of {@link GetImageExifMetadata }
     * 
     */
    public GetImageExifMetadata createGetImageExifMetadata() {
        return new GetImageExifMetadata();
    }

    /**
     * Create an instance of {@link GetImageExifMetadataResponse }
     * 
     */
    public GetImageExifMetadataResponse createGetImageExifMetadataResponse() {
        return new GetImageExifMetadataResponse();
    }

    /**
     * Create an instance of {@link CreateLoginSession }
     * 
     */
    public CreateLoginSession createCreateLoginSession() {
        return new CreateLoginSession();
    }

    /**
     * Create an instance of {@link CreateLoginSessionResponse }
     * 
     */
    public CreateLoginSessionResponse createCreateLoginSessionResponse() {
        return new CreateLoginSessionResponse();
    }

}
