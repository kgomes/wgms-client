package org.mbari.odss.waveglider.client;

import java.io.IOException;
import java.util.logging.Logger;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class AMQPClient {

	/**
	 * The logger
	 */
	private static final Logger logger = Logger.getLogger("AMQPClient");

	/**
	 * The server name that this client will interact with
	 */
	private String serverHostame = null;

	/**
	 * The port on the server to connect to
	 */
	private Integer serverPort = null;

	/**
	 * The virtual host to use
	 */
	private String vhost = null;

	/**
	 * The username that will be used to connect to the server
	 */
	private String username = null;

	/**
	 * The password used to connect
	 */
	private String password = null;

	/**
	 * The connection to the server
	 */
	private Connection connection = null;

	/**
	 * The AMQP channel that this client will use to communicate
	 */
	private Channel channel = null;

	/**
	 * A flag to indicate if the client is connected or not
	 */
	private boolean connected = false;

	/**
	 * The constructor
	 * 
	 * @param serverHostname
	 * @param serverPort
	 * @param vhost
	 * @param username
	 * @param password
	 */
	public AMQPClient(String serverHostname, Integer serverPort, String vhost,
			String username, String password) {
		// Assign the parameters and check for valid values
		if (serverHostname != null) {
			this.serverHostame = serverHostname;
		} else {
			throw new IllegalArgumentException(
					"Server name needs to be defined");
		}
		if (serverPort != null) {
			this.serverPort = serverPort;
		} else {
			// Assign the default RabbitMQ port
			serverPort = 5672;
		}
		if (vhost != null) {
			this.vhost = vhost;
		} else {
			throw new IllegalArgumentException(
					"Need to define the virtual host");
		}
		this.username = username;
		this.password = password;
	}

	/**
	 * The method to return the connection status
	 * 
	 * @return
	 */
	public boolean isConnected() {
		return connected;
	}

	/**
	 * This method connects to the AMQP server and establishes a channel that
	 * will be use to send messages
	 */
	public void connectToServer() {
		// Grab the connection factory
		ConnectionFactory factory = new ConnectionFactory();

		// Set the connection parameters
		factory.setUsername(username);
		factory.setPassword(password);
		factory.setVirtualHost(vhost);
		factory.setHost(serverHostame);
		factory.setPort(serverPort);

		// Now connect to the server
		try {
			connection = factory.newConnection();
		} catch (IOException e) {
			logger.severe("IOException caught trying to "
					+ "connect to the AMQP server: " + e.getMessage());
		}

		// Now grab a channel if the connection is OK
		if (connection != null) {
			try {
				channel = connection.createChannel();
				connected = true;
			} catch (IOException e) {
				logger.severe("IOException caught trying to "
						+ "connect to get a channel from the AMQP server: "
						+ e.getMessage());
				connected = false;
			}
		}
	}

	/**
	 * This method publishes a message to an exchange (this assume the exchange
	 * is a fanout exchange)
	 * 
	 * @param messagePayload
	 * @param exchange
	 */
	public void publishMessageToFanoutExchange(byte[] messagePayload,
			String exchange) {
		if (connected) {
			try {
				channel.basicPublish(exchange, "", null, messagePayload);
			} catch (IOException e) {
				logger.severe("IOException pubishing message to exchange "
						+ exchange + ": " + e.getMessage());
			}
		}
	}

	/**
	 * This method declares a topic exchange with the given name
	 * 
	 * @param exchange
	 */
	public void declareTopicExchange(String exchange) {
		if (connected) {
			try {
				channel.exchangeDeclare(exchange, "topic", true);
			} catch (IOException e) {
				logger.severe("IOException declaring exchange " + exchange
						+ ": " + e.getMessage());
			}
		}
	}

	/**
	 * This message publishes the message to a Topic exchange using the given
	 * routing key
	 * 
	 * @param messagePayload
	 * @param exchange
	 * @param routingKey
	 */
	public void publishMessageToTopicExchange(byte[] messagePayload,
			String exchange, String routingKey) {
		if (connected) {
			try {
				channel.exchangeDeclare(exchange, "topic", true);
				channel.basicPublish(exchange, routingKey, null, messagePayload);
			} catch (IOException e) {
				logger.severe("IOException publishing message to topic exchange "
						+ exchange + ": " + e.getMessage());
			}
		}
	}

	/**
	 * This method does any cleanup with the AMQP server
	 */
	public void cleanup() {
		if (connected) {
			try {
				channel.close();
				channel = null;
				connection.close();
				connection = null;
			} catch (IOException e) {
				logger.severe("IOException caught trying to close the "
						+ "connection and channel to the server: "
						+ e.getMessage());
			}
			connected = false;
		}
	}
}
