package org.mbari.odss.waveglider.client;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class WGMSClient {
    /**
     * The logger
     */
    private static final Logger logger = Logger.getLogger("WGMSClient");

    /**
     * The service that we will be using
     */
    private static final QName SERVICE_NAME = new QName(
            "http://gliders.liquidr.com/webservicesWebServices", "EntityAPI");

    /**
     * The URL of the service that this client will be interacting with
     */
    private String serviceUrl = null;

    /**
     * The username that will be used to authenticate against the WGMS server
     */
    private String username = null;

    /**
     * The password that will be used to authenticate against the WGMS server
     */
    private String password = null;

    /**
     * The organization name that will be used to authenticate against hte WGMS
     * server
     */
    private String orgName = null;

    /**
     * The ID of the vehicle that we will be dealing with
     */
    private Integer vehicleId = null;

    /**
     * The entity type of the vehicle
     */
    private Integer entityType = null;

    /**
     * Not sure what a related entity type is, we just need it for some things
     */
    private Integer relatedEntityType = null;

    /**
     * The port of the WGMS web service that will be used to call methods. This
     * need to be objects because they may be different depending on the server
     * being used
     */
    private Object port = null;

    /**
     * The client object that is interfacing with the server
     */
    private Client cxfClient = null;

    /**
     * A flag to indicate if the web services client was able to authenticate OK
     */
    private boolean authenticated = false;

    /**
     * The constructor
     *
     * @param serviceUrl
     * @param username
     * @param password
     * @param orgName
     * @param vehicleId
     * @param entityType
     * @param relatedEntityType
     */
    public WGMSClient(String serviceUrl, String username, String password,
                      String orgName, Integer vehicleId, Integer entityType,
                      Integer relatedEntityType) {
        // Assign local variables and do some sanity checks
        if (serviceUrl != null) {
            this.serviceUrl = serviceUrl;
        } else {
            throw new IllegalArgumentException("ServiceURL needs to be defined");
        }
        if (username != null) {
            this.username = username;
        } else {
            throw new IllegalArgumentException("Username needs to be defined");
        }
        if (password != null) {
            this.password = password;
        } else {
            throw new IllegalArgumentException("Password needs to be defined");
        }
        if (orgName != null) {
            this.orgName = orgName;
        } else {
            throw new IllegalArgumentException("orgName needs to be defined");
        }
        if (vehicleId != null) {
            this.vehicleId = vehicleId;
        } else {
            throw new IllegalArgumentException("vehicleId needs to be defined");
        }
        if (entityType != null) {
            this.entityType = entityType;
        } else {
            throw new IllegalArgumentException("entityType needs to be defined");
        }
        this.relatedEntityType = relatedEntityType;

        // Now depending on the service URL, grab the correct port and client
        if (serviceUrl.startsWith("https://mbari.")) {
            try {
                // Grab the URL of the service from the CXF generated API
                URL wsdlURL = org.mbari.odss.waveglider.client.wgms.mbari.EntityAPI.WSDL_LOCATION;
                logger.info("wsdlURL: " + wsdlURL);

                // Now grab the service
                // com.liquidr.gliders.webserviceswebservices.EntityAPI ss = new
                // com.liquidr.gliders.webserviceswebservices.EntityAPI(
                // wsdlURL, SERVICE_NAME);
                org.mbari.odss.waveglider.client.wgms.mbari.EntityAPI ss = new org.mbari.odss.waveglider.client.wgms.mbari.EntityAPI();
                logger.info("Got EntityAPI");

                // And the SOAP 1.2 port associated with the service
                port = ss.getEntityAPISoap12();
                logger.info("Got port");

                // Grab the client class for later
                cxfClient = ClientProxy.getClient(port);
                logger.info("Got cxfClient");
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else if (serviceUrl.startsWith("https://vr2ctest.")) {
            try {
                // Grab the URL of the service from the CXF generated API
                URL wsdlURL = org.mbari.odss.waveglider.client.wgms.vr2ctest.EntityAPI.WSDL_LOCATION;
                logger.info("wsdlURL: " + wsdlURL);

                // Now grab the service
                org.mbari.odss.waveglider.client.wgms.vr2ctest.EntityAPI ss = new org.mbari.odss.waveglider.client.wgms.vr2ctest.EntityAPI();
                logger.info("Got EntityAPI");

                // And the SOAP 1.2 port associated with the service
                port = ss.getEntityAPISoap12();
                logger.info("Got port");

                // Grab the client class for later
                cxfClient = ClientProxy.getClient(port);
                logger.info("Got cxfClient");
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else {
            throw new IllegalArgumentException("The service URL " + serviceUrl
                    + " points to a service that has not been integrated "
                    + "into this client.  A new Java package will have to be "
                    + "generated that points to that service URL and then "
                    + "integrated into this client code.  Sorry about that.");
        }
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    /**
     * This method attempts to login to the WGMS web service and then stores the
     * authentication cookie name and value in both local variables and in the
     * responseHeader
     */
    public void loginToService() {

        logger.fine("Invoking loginToService...");

        // Call the service method to login
        String loginResult = null;

        if (serviceUrl.startsWith("https://mbari.")) {
            loginResult = ((org.mbari.odss.waveglider.client.wgms.mbari.EntityAPISoap) port)
                    .createLoginSession(username, password, orgName);
        } else if (serviceUrl.startsWith("https://vr2ctest.")){
            loginResult = ((org.mbari.odss.waveglider.client.wgms.vr2ctest.EntityAPISoap) port)
                    .createLoginSession(username, password, orgName);
        }
        logger.info("Login result: " + loginResult);

        // Make sure login result was successful
        if (loginResult != null && !loginResult.equals("-1")) {

            // Grab the response context so we can look at the response from the
            // server
            Map<String, Object> responseContext = cxfClient
                    .getResponseContext();
            for (Iterator<String> iterator = responseContext.keySet()
                    .iterator(); iterator.hasNext(); ) {
                String key = iterator.next();
                logger.fine("Key: " + key + ", value: "
                        + responseContext.get(key));
            }

            // Grab the map of protocol headers
            @SuppressWarnings("unchecked")
            TreeMap<String, Object> protocolHeaders = (TreeMap<String, Object>) responseContext
                    .get("org.apache.cxf.message.Message.PROTOCOL_HEADERS");

            // Grab the Collection associated with the key "Set-Cookie" to look
            // for cookies the server wants us to set
            @SuppressWarnings("unchecked")
            Collection<String> cookieCollection = (Collection<String>) protocolHeaders
                    .get("Set-Cookie");
            logger.fine("All cookies => " + cookieCollection);

            // Take those cookies, add them to a TreeMap with a name of "Cookie"
            TreeMap<String, Object> cookieTreeMap = new TreeMap<String, Object>();
            cookieTreeMap.put("Cookie", cookieCollection);

            // Now add those as the PROTOCOL_HEADERS on the requests to be made
            // in the future
            cxfClient.getRequestContext().put(
                    "org.apache.cxf.message.Message.PROTOCOL_HEADERS",
                    cookieTreeMap);

            // If the login result was not -1 and there were Set-cookies, set
            // the authenticated flag
            if (!loginResult.equals("-1") && cookieCollection != null
                    && cookieCollection.size() > 0)
                authenticated = true;
        } else {
            logger.severe("Login failed failed for some reason");
            throw new RuntimeException("Login failed for some reason");
        }
    }

    /**
     * This method takes in a number that indicates how many position records
     * are to be read from the WGMS service and then returns a TreeMap with key
     * of Dates and values of an array of Doubles that indicate the lat and lon
     * of the vehicle positions
     */
    public TreeMap<Date, Double[]> readVehiclePosition(int numberOfRecordsToRead) {

        logger.fine("readVehiclePositions called");
        // The TreeMap to return
        TreeMap<Date, Double[]> positionTreeMap = new TreeMap<Date, Double[]>();

        // Check for authenticated session
        if (authenticated) {

            // Create the columnXML
            String columnXMLForVehiclePosition = "<columns>"
                    + "<column type='id'>VehicleParsedOutputId</column>"
                    + "<column type='bound'>TimeStamp</column>"
                    + "<column type='bound'>Current_Latitude</column>"
                    + "<column type='bound'>Current_Longitude</column>"
                    + "<column type='bound'>Heading_Sub</column>"
                    + "<column type='bound'>Desired_Bearing</column>"
                    + "<column type='bound'>Glider_Heading</column>"
                    + "<column type='bound'>Glider_Speed</column>"
                    + "</columns>";

            // Call the method to get the position
            String positionResult = null;

            if (serviceUrl.startsWith("https://mbari.")) {
                positionResult = ((org.mbari.odss.waveglider.client.wgms.mbari.EntityAPISoap) port)
                        .getRelatedEntitySet(entityType, vehicleId,
                                relatedEntityType, columnXMLForVehiclePosition,
                                1, numberOfRecordsToRead, "Timestamp", "DESC",
                                "");
                logger.fine("Position Result: " + positionResult);
            } else if (serviceUrl.startsWith("https://vr2ctest.")) {
                positionResult = ((org.mbari.odss.waveglider.client.wgms.vr2ctest.EntityAPISoap) port)
                        .getRelatedEntitySet(entityType, vehicleId,
                                relatedEntityType, columnXMLForVehiclePosition,
                                1, numberOfRecordsToRead, "Timestamp", "DESC",
                                "");
                logger.fine("Position Result: " + positionResult);
            }
            // Check the result
            if (positionResult != null) {

                // The return should be XML so we will need to process in that
                // way
                DocumentBuilderFactory dbf = DocumentBuilderFactory
                        .newInstance();
                DocumentBuilder db = null;
                try {
                    db = dbf.newDocumentBuilder();
                } catch (ParserConfigurationException e) {
                    logger.severe("ParserConfigurationException caught trying "
                            + "to get XML parser set up for readVehiclePosition call: "
                            + e.getMessage());
                }

                // If builder is OK, read in the XML from the reply into a
                // Document object
                if (db != null) {
                    InputSource is = new InputSource(new StringReader(
                            positionResult));
                    Document doc = null;
                    try {
                        doc = db.parse(is);
                    } catch (SAXException e) {
                        logger.severe("SAXException caught trying to parse "
                                + "the XML returned from readVehiclePosition: "
                                + e.getMessage());
                    } catch (IOException e) {
                        logger.severe("IOException caught trying to parse "
                                + "the XML returned from readVehiclePosition: "
                                + e.getMessage());
                    }

                    // If the document was constructed OK
                    if (doc != null) {
                        // Grab list of "Table1" elements
                        NodeList table1Elements = doc
                                .getElementsByTagName("Table1");

                        // Date format for parsing
                        DateFormat dateFormat = new SimpleDateFormat(
                                "M/d/yyyy H:mm");
                        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

                        DateFormat dateFormat2 = new SimpleDateFormat(
                                "yyyy/M/d HH:mm");
                        dateFormat2.setTimeZone(TimeZone.getTimeZone("GMT"));

                        // Now for each Table1 element, grab the timestamp, lat
                        // and lon
                        for (int i = 0; i < table1Elements.getLength(); i++) {
                            // Grab the Table1 element
                            Element tableElement = (Element) table1Elements
                                    .item(i);

                            // Grab the timestamp element
                            Element timestampElement = (Element) tableElement
                                    .getElementsByTagName("TimeStamp").item(0);
                            NodeList timestampNodes = timestampElement
                                    .getChildNodes();
                            String timestampString = ((Node) timestampNodes
                                    .item(0)).getNodeValue();

                            // Grab the latitude element
                            Element latitudeElement = (Element) tableElement
                                    .getElementsByTagName("Current_Latitude")
                                    .item(0);
                            NodeList latitudeNodes = latitudeElement
                                    .getChildNodes();
                            String latitudeString = ((Node) latitudeNodes
                                    .item(0)).getNodeValue();

                            // Grab the longitude element
                            Element longitudeElement = (Element) tableElement
                                    .getElementsByTagName("Current_Longitude")
                                    .item(0);
                            NodeList longitudeNodes = longitudeElement
                                    .getChildNodes();
                            String longitudeString = ((Node) longitudeNodes
                                    .item(0)).getNodeValue();
                            logger.finer("From XML response->Time: "
                                    + timestampString + ", lat = "
                                    + latitudeString + ", lon = "
                                    + longitudeString);

                            // Try to parse the date time using the first date format
                            Date timestamp = null;
                            try {
                                timestamp = dateFormat.parse(timestampString);
                            } catch (ParseException e) {
                                logger.info("Could not parse date "
                                        + timestampString
                                        + " into a valid date using first date format, will try second");
                            }

                            // If the timestamp wasn't parsed, try the second format
                            if (timestamp == null) {
                                try {
                                    timestamp = dateFormat2.parse(timestampString);
                                } catch (ParseException e) {
                                    logger.severe("Could not parse date "
                                            + timestampString
                                            + " into a valid date using second format either");
                                }
                            } else {
                                // Check to make sure the parsing was correct (check year)
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(timestamp);
                                if (calendar.get(Calendar.YEAR) < 1970){
                                    try {
                                        timestamp = dateFormat2.parse(timestampString);
                                    } catch (ParseException e) {
                                        logger.severe("Could not parse date "
                                                + timestampString
                                                + " into a valid date using second format either");
                                    }
                                }
                            }
                            // Make sure we have a valid date
                            if (timestamp != null) {
                                // Set seconds and
                                // Now parse lat and lon
                                Double latitude = null;
                                Double longitude = null;
                                try {
                                    latitude = Double
                                            .parseDouble(latitudeString);
                                    longitude = Double
                                            .parseDouble(longitudeString);
                                } catch (NumberFormatException e) {
                                    logger.severe("NumberFormatException parsing lat and lon of: "
                                            + latitudeString
                                            + ", "
                                            + longitudeString);
                                }
                                if (latitude != null && longitude != null) {
                                    logger.fine("Date: "
                                            + dateFormat.format(timestamp)
                                            + " lat = " + latitude + ", lon = "
                                            + longitude);
                                    Double[] latlons = {latitude, longitude};
                                    positionTreeMap.put(timestamp, latlons);
                                }
                            }
                        }
                    }
                }
            } else {
                logger.severe("Nothing (null) came back from the call the get vehicle positions");
            }
        } else {
            throw new IllegalStateException("Not logged in to web service.");
        }

        // Return results
        return positionTreeMap;
    }

    /**
     * This method reads the CTD records and returns them in TreeMap form:
     * <p/>
     * <pre>
     * Key: Date (timestamp)
     * Value: Double [Pressure, Temperature, Conductivity]
     * </pre>
     */
    public TreeMap<Date, Double[]> readCTDData(int numberOfRecordsToRead) {
        // Create the treemap to return
        TreeMap<Date, Double[]> ctdData = new TreeMap<Date, Double[]>();

        // Construct the XML for the data to return
        String columnXMLForCTDData = "<columns>"
                + "<column type='id'>PayloadPacketRecordId</column>"
                + "<column type='bound'>TimeStamp</column>"
                + "<column type='bound'>Latitude</column>"
                + "<column type='bound'>Longitude</column>"
                + "<column type='calculated' method='bytefield|int|0|4|0.01|-10' name='Pressure'>RecordData</column>"
                + "<column type='calculated' method='bytefield|int|4|4|0.0001|-5' name='Temperature'>RecordData</column>"
                + "<column type='calculated' method='bytefield|int|8|4|0.00001|-0.05' name='Conductivity'>RecordData</column>"
                + "<column type='calculated'  method='bytefield|int|12|4|0.1|0' name='Oxygen'>RecordData</column>"
                + "<column type='bound' name='Payload Data'>RecordData</column>"
                + "</columns>";

        // Construct the XML for the query criteria
        String criteriaXML = "<CriteriaXml>"
                + "<Criteria>"
                + "<Column id='35261'>VehicleId</Column>"
                + "<conditionVehicleId>0</conditionVehicleId>"
                + "<value1VehicleId type='48'>352</value1VehicleId>"
                + "</Criteria>"
                // + "<Criteria>"
                // + "<Column id='35262'>TimeStamp</Column>"
                // + "<conditionTimeStamp>13</conditionTimeStamp>"
                // + "<value1TimeStamp>2012-06-05T00:00:00</value1TimeStamp>"
                // + "<value2TimeStamp>2012-06-05T23:59:59</value2TimeStamp>"
                // + "</Criteria>"
                + "<Criteria>" + "<Column id='45997'>PayloadType</Column>"
                + "<conditionPayloadType>0</conditionPayloadType>"
                + "<value1PayloadType>112</value1PayloadType>"
                + "<value2PayloadType>" + "</value2PayloadType>"
                + "</Criteria>" + "<Criteria>" + "<Column id='undefined'>"
                + "</Column>" + "</Criteria>" + "</CriteriaXml>";

        // Run the query
        String ctdResult = null;
        if (serviceUrl.startsWith("https://mbari.")) {
            ctdResult = ((org.mbari.odss.waveglider.client.wgms.mbari.EntityAPISoap) port)
                    .getEntitySetWithCriteria(94, columnXMLForCTDData, 1,
                            numberOfRecordsToRead, "TimeStamp", " DESC",
                            criteriaXML);
        } else if (serviceUrl.startsWith("https://vr2ctest.")) {
            ctdResult = ((org.mbari.odss.waveglider.client.wgms.vr2ctest.EntityAPISoap) port)
                    .getEntitySetWithCriteria(94, columnXMLForCTDData, 1,
                            numberOfRecordsToRead, "TimeStamp", " DESC",
                            criteriaXML);
        }
        logger.fine("CTD Result: " + ctdResult);

        // The return should be XML so we will need to process in that way
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.severe("ParserConfigurationException caught trying "
                    + "to get XML parser set up for readVehiclePosition call: "
                    + e.getMessage());
        }

        // If builder is OK, read in the XML from the reply into a Document
        // object
        if (db != null) {
            InputSource is = new InputSource(new StringReader(ctdResult));
            Document doc = null;
            try {
                doc = db.parse(is);
            } catch (SAXException e) {
                logger.severe("SAXException caught trying to parse "
                        + "the XML returned from readVehiclePosition: "
                        + e.getMessage());
            } catch (IOException e) {
                logger.severe("IOException caught trying to parse "
                        + "the XML returned from readVehiclePosition: "
                        + e.getMessage());
            }

            // If the document was constructed OK
            if (doc != null) {
                // Grab list of "Table1" elements
                NodeList table1Elements = doc.getElementsByTagName("Table1");

                // Date format for parsing
                DateFormat dateFormat = new SimpleDateFormat("M/d/yyyy h:m a");
                dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

                // Now for each Table1 element, grab the TimeStamp,
                // RecordData(Pressure), RecordData1(Temperature),
                // RecordData2(Conductivity)
                for (int i = 0; i < table1Elements.getLength(); i++) {
                    // Grab the Table1 element
                    Element tableElement = (Element) table1Elements.item(i);

                    // Grab the timestamp element
                    Element timestampElement = (Element) tableElement
                            .getElementsByTagName("TimeStamp").item(0);
                    NodeList timestampNodes = timestampElement.getChildNodes();
                    String timestampString = ((Node) timestampNodes.item(0))
                            .getNodeValue();

                    // Grab the latitude
                    Element latitudeElement = (Element) tableElement
                            .getElementsByTagName("Latitude").item(0);
                    NodeList latitudeNodes = latitudeElement.getChildNodes();
                    String latitudeString = ((Node) latitudeNodes.item(0))
                            .getNodeValue();

                    // Grab the longitude
                    Element longitudeElement = (Element) tableElement
                            .getElementsByTagName("Longitude").item(0);
                    NodeList longitudeNodes = longitudeElement.getChildNodes();
                    String longitudeString = ((Node) latitudeNodes.item(0))
                            .getNodeValue();

                    // Grab the pressure
                    Element pressureElement = (Element) tableElement
                            .getElementsByTagName("RecordData").item(0);
                    NodeList pressureNodes = pressureElement.getChildNodes();
                    String pressureString = ((Node) pressureNodes.item(0))
                            .getNodeValue();

                    // Temperature
                    Element temperatureElement = (Element) tableElement
                            .getElementsByTagName("RecordData1").item(0);
                    NodeList temperatureNodes = temperatureElement
                            .getChildNodes();
                    String temperatureString = ((Node) temperatureNodes.item(0))
                            .getNodeValue();

                    // Conductivity
                    Element conductivityElement = (Element) tableElement
                            .getElementsByTagName("RecordData2").item(0);
                    NodeList conductivityNodes = conductivityElement
                            .getChildNodes();
                    String conductivityString = ((Node) conductivityNodes
                            .item(0)).getNodeValue();

                    logger.fine("Time: " + timestampString + ", pressure = "
                            + pressureString + ", temperature = "
                            + temperatureString + ", conductivity = "
                            + conductivityString);

                    // Convert to date and doubles
                    Date timestamp = null;
                    Double latitude = null;
                    Double longitude = null;
                    Double pressure = null;
                    Double temperature = null;
                    Double conductivity = null;
                    try {
                        timestamp = dateFormat.parse(timestampString);
                        latitude = Double.parseDouble(latitudeString);
                        longitude = Double.parseDouble(longitudeString);
                        conductivity = Double.parseDouble(conductivityString);
                        temperature = Double.parseDouble(temperatureString);
                        pressure = Double.parseDouble(pressureString);
                    } catch (NumberFormatException e) {
                        logger.severe("NumberFormatException caught trying to "
                                + "convert the pressure, temperature and conductivity values: "
                                + e.getMessage());
                    } catch (ParseException e) {
                        logger.severe("ParseException caught trying to "
                                + "convert the timestamp values: "
                                + e.getMessage());
                    }
                    if (timestamp != null && latitude != null
                            && longitude != null && pressure != null
                            && temperature != null && conductivity != null) {
                        Double[] ctdValues = {latitude, longitude,
                                conductivity, temperature, pressure};
                        ctdData.put(timestamp, ctdValues);
                    }
                }
            }
        }

        // Return the map
        return ctdData;
    }
}
