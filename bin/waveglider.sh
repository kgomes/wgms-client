#!/bin/sh
export JAVA_HOME=/usr/java/latest
export PATH=$JAVA_HOME/bin:$PATH
java -Djavax.net.ssl.trustStore=mbari.wgms.keystore -Djavax.net.ssl.keyStorePassword=wgmsclient -Djava.util.logging.config.file=wgms_client.properties -jar wave-glider.jar wgms_client.properties